using System;
using sistemaBancario;
using sistemaBancario.Conta;  

namespace sistemaBancario.Conta.ContaCorrente {
    public class ContaCorrente : Conta {
        //Declaração dos atributos        

        private double saldo;
        private String nome;
        private String cpf;
        private Int telefone;

        public  void criarContaCorrente(string nome, string cpf, int telefone)
        {
            this.nome = nome;
            this.cpf = cpf;
            this.telefone = telefone;
            this.saldo = 0;
        }
            
        public double getSaldo() { return saldo; }

        override public double obterSaldo(){
            return getSaldo();
        }

        override public void sacar(double valor) {

            if(valor > 0 && valor != null && saldo >= valor){

                        saldo -= valor;

            }
        }
        
        override public void depositar(double valor) {

            if(valor > 0 && valor != null){

                    saldo +=valor;
            }
            
        }
        

    }
}