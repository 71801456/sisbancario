using System;
using sistemaBancario; 

namespace sistemaBancario.Conta {
    public abstract class Conta {

        //Declaração de atributo da classe abstrata Conta
        private double saldo;

        //Métodos acessores
        public abstract double obterSaldo();

        public virtual void sacar(double valor){}

        public virtual void depositar(double valor){}

    }
}